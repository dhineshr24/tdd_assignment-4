from random import randint

class calculator:
    def add(self,num1,num2):
        if(num1=='random'):
            return randint(1,8)+num2
        elif (type(num1)!=int or type(num2)!=int):
            return "invalid input"
        else:
            return num1+num2
    def substract(self,num1,num2):
        if(num1=="random"):
            return randint(1,8)-num2
        elif(type(num1)!=int or type(num2)!=int):
            return "invalid input"
        else:
            return num1-num2
    def multiply(self,num1,num2):
        if(num1=="random"):
            return randint(1,8)*num2
        elif(type(num1)!=int or type(num2)!=int):
            return "invalid input"
        else:
            return num1*num2
    def divide(self,num1,num2):
        if(num2==0):
            return "Zero Division Error"
        elif(num1=="random"):
            return randint(1,8)/num2
        elif(type(num1)!=int or type(num2)!=int):
            return "invalid input"
        else:
            return num1/num2
